package ocr;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author Drobny
 */
public class Training {
    final static String pathNumbers = "./resources/ocr/numbers/";
    final static String pathNumbersBig = "./resources/ocr/numbers/big/";
    final static String pathAll = "./resources/ocr/all/";
    final static String pathAllBig = "./resources/ocr/all/big/";
    final String trainingDataNumbersFileName = "trainingDataNumbers.ser";
    final String trainingDataFileName = "trainingDataAll.ser";
    
    final boolean TRAINING_DATA_FROM_SER_FILE = true;
    
    public List<FeaturesVector> trainingVectorsAll = new ArrayList();
    public List<FeaturesVector> trainingVectorsNumbers = new ArrayList();
    
    public Training() {
        if(TRAINING_DATA_FROM_SER_FILE) {
            getDataFromFile();
        } else {
            extractionFromImages("numbers");
            extractionFromImages("all");
        }
    }
    
    /** 
     * Read training data from "trainingData.dat" file
     */
    private void getDataFromFile() {
        try {
            FileInputStream fileIn = new FileInputStream(pathNumbers +"../"+ trainingDataNumbersFileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            trainingVectorsNumbers = (List<FeaturesVector>) in.readObject();
            in.close();
            
            fileIn = new FileInputStream(pathAll +"../"+ trainingDataFileName);
            in = new ObjectInputStream(fileIn);
            trainingVectorsAll = (List<FeaturesVector>) in.readObject();
            in.close();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public final void extractionFromImages(String type) {
        String fileName, sign;
        String[] fileNameSplit;
        Mark mark;
        Mat markImage;
        File folder;
        File[] files;
        File tmpImageFile;
        
        String path, pathBig, dataFileName;
        List<FeaturesVector> trainingVectors;
        if(type.equals("numbers")) {
            path = pathNumbers;
            pathBig = pathNumbersBig;
            trainingVectors = trainingVectorsNumbers;
            dataFileName = trainingDataNumbersFileName;
        } else {
            path = pathAll;
            pathBig = pathAllBig;
            trainingVectors = trainingVectorsAll;
            dataFileName = trainingDataFileName;
        }
        
        folder = new File(pathBig);
        files = folder.listFiles(); 
        
        for (File file : files) {
            if(file.isDirectory()) continue;
            fileName = file.getName();
            fileNameSplit = fileName.split("_");
            sign = fileNameSplit[0];
            
            tmpImageFile = new File(pathBig + fileName);
            markImage = Highgui.imread(tmpImageFile.getAbsolutePath());
            
            if(markImage.size().area() > 0) {
                resizeAndSaveImage(markImage, OcrProcess.trainingImageSize, path + fileName);
            
                mark = new Mark(markImage);
                /** get features points vector **/
                mark.extractMark();
                mark.featuresVector.setClassName(sign);
                trainingVectors.add(mark.featuresVector);
            }
        }
        
        try {
            FileOutputStream fileOut = new FileOutputStream(path +"../"+ dataFileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(trainingVectors);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public void resizeAndSaveImage(Mat image, int maxHeight, String pathAndFileName) {
        double factor = image.width() / (double) image.height();
        Size dsize = new Size((int) (maxHeight * factor), maxHeight);
        
        Imgproc.resize(image, image, dsize);
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);
        Imgproc.threshold(image, image, 254, 255, Imgproc.THRESH_BINARY);
        
        File tmpImageFile = new File(pathAndFileName);
        Highgui.imwrite(tmpImageFile.getAbsolutePath() , image);
    }
}
