/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import java.awt.image.BufferedImage;
import java.io.File;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 * Loads static image to OpenCV Mat object Converts Mat to BufferedImage
 *
 * @author Drobny
 */
public class ImageManager {

    public ImageManager() {
    }

    public static Mat loadImageMat(String fileName) {
        File imageFile = new File( fileName);
        return Highgui.imread(imageFile.getAbsolutePath());
    }

    public static void saveImageMat(String fileName, Mat img) {
        File imageFile = new File(fileName);
        Highgui.imwrite(imageFile.getAbsolutePath(), img);
    }

    /**
     * Converts image in Mat to BufferedImage
     *
     * @param in
     * @return BufferedImage
     */
    public static BufferedImage mat2Img(Mat in) {
        BufferedImage out;
        int width, height;
        width = in.width();
        height = in.height();

        //Added because Bufferedimage has only BYTE_BGR
        if (in.channels() > 1) {
            Imgproc.cvtColor(in, in, Imgproc.COLOR_RGB2BGR);
        }

        byte[] data = new byte[width * height * (int) in.elemSize()];
        int type;
        in.get(0, 0, data);

        if (in.channels() == 1) {
            type = BufferedImage.TYPE_BYTE_GRAY;
        } else {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        out = new BufferedImage(width, height, type);

        out.getRaster().setDataElements(0, 0, width, height, data);

        return out;
    }
    
}
