package app;

import app.exception.GDDException;
import app.model.PlotModel;
import app.preprocessing.Preprocessing;
import app.processes.ColourSegmentation;
import app.view.FXMLView;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import app.processes.ScaleConverter;
import app.processes.PlotAreaDetectorProcess;
import app.processes.PlotValuesReaderProcess;
import app.processes.DescriptionDetector;
import app.processes.TicksDetectorProcess;
import java.util.ArrayList;
import ocr.OcrProcess;
import utils.ImageManager;

/**
 * Main class 
 * 
 * @author Drobny
 */
public class GraphDigitizerDesktop extends Application {

    private FXMLView fxmlView;

    public Mat resultImage;
    public BufferedImage sourceImageBuffered;
    public PlotModel plotModel;
    public OcrProcess ocr;
    
    private static final String FXML_LOCATION = "view/FXMLDocument.fxml";

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_LOCATION));

                File step4=new File("resources/trash");
        if (!step4.exists())
            step4.mkdirs();

        
        Parent root = (Parent) fxmlLoader.load();
        Scene scene = new Scene(root);

        stage.setTitle("Graph Digitizer");
        stage.setScene(scene);
        stage.show();

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        fxmlView = fxmlLoader.getController();
        fxmlView.setStage(stage);
        fxmlView.setGdd(this);
        
        ocr = new OcrProcess();
    }

    /**
     * Loads image from file and creates new plot model
     *
     * @param fileName
     */
    public void loadImage(String fileName) {
        Mat sourceImage = ImageManager.loadImageMat(fileName);
        BufferedImage sourceImageBuffered = ImageManager.mat2Img(sourceImage);
        fxmlView.setImageFromBuffered(sourceImageBuffered);
        plotModel = new PlotModel(sourceImage);
    }

    /**
     * Reading points from plot
     */
    public void process() throws GDDException{
        
        plotModel.setPlotType(fxmlView.getPlotType());
        if(fxmlView.hasLegend() && !plotModel.isLegendReaded()){
            throw new GDDException("If plot has legend read it.");
        }

        /**
         * Process starts here *
         */
        resultImage = new Mat();

        Rect areaRect = plotModel.getAreaRect();
        /* Reduce plot area dimensions - cut axis. It has to be done in other way (not hardcoded 2px) */
        Rect plotRect = new Rect(new Point(areaRect.x + 2, areaRect.y + 2),
                new Point(areaRect.x + areaRect.width - 2, areaRect.y + areaRect.height - 2));

        plotModel.setAreaRect(plotRect);
        PlotValuesReaderProcess.process(plotModel);

        savePlotPoints(areaRect);

        double xMin = fxmlView.getXMinValue();
        double xMax = fxmlView.getXMaxValue();
        double yMin = fxmlView.getYMinValue();
        double yMax = fxmlView.getYMaxValue();

        ScaleConverter.countCoefficients(plotModel, xMin, xMax, yMin, yMax);
        
        fxmlView.fillPlotCombobox(plotModel.getPlotsInLegend());
    }

    /**
     * Preprocessing for reading plot: - detects plot area - detects ticks -
     * detects rectangles with axis (x,y) values (min/max) and titles
     */
    public void findIntrestingAreasOnImage() {
        
        resultImage = new Mat();
        PlotAreaDetectorProcess.process(plotModel);

        TicksDetectorProcess.process(plotModel);

        List<Point> xTicksList = plotModel.getxTicksList();
        List<Point> yTicksList = plotModel.getyTicksList();

        int xListS = xTicksList.size();
        int yListS = yTicksList.size();
        double[] xLoc = new double[xListS];
        double[] yLoc = new double[yListS];

        for (int i = 0; i < xListS; ++i) {
            xLoc[i] = xTicksList.get(i).x;
        }
        for (int i = 0; i < yListS; ++i) {
            yLoc[i] = yTicksList.get(i).y;
        }
        Arrays.sort(xLoc);
        Arrays.sort(yLoc);
        Rect plotAreaRect = plotModel.getAreaRect();
        DescriptionDetector descDetector = new DescriptionDetector(plotModel.getSourceImage());
        Rect xminR = null, xmaxR = null, yminR = null, ymaxR = null;
        try{
            Point xmin = new Point(xLoc[0] + plotAreaRect.x, plotAreaRect.y + plotAreaRect.height);
            Point xmax = new Point(xLoc[xListS - 1] + plotAreaRect.x, plotAreaRect.y + plotAreaRect.height);
            Point ymin = new Point(0 + plotAreaRect.x, yLoc[yListS - 1] + plotAreaRect.y);
            Point ymax = new Point(0 + plotAreaRect.x, yLoc[0] + plotAreaRect.y);
            
            xminR = descDetector.detectScaleDescription(
                xmin, DescriptionDetector.SCALE_X);
            xmaxR = descDetector.detectScaleDescription(
                    xmax, DescriptionDetector.SCALE_X);
            yminR = descDetector.detectScaleDescription(
                    ymin, DescriptionDetector.SCALE_Y);
            ymaxR = descDetector.detectScaleDescription(
                    ymax, DescriptionDetector.SCALE_Y);
            fxmlView.setDescriptionRectangle(xminR.x, xminR.y, xminR.width, xminR.height,
                FXMLView.TEXT_X_MIN);
            fxmlView.setDescriptionRectangle(xmaxR.x, xmaxR.y, xmaxR.width, xmaxR.height,
                    FXMLView.TEXT_X_MAX);
            fxmlView.setDescriptionRectangle(yminR.x, yminR.y, yminR.width, yminR.height,
                    FXMLView.TEXT_Y_MIN);
            fxmlView.setDescriptionRectangle(ymaxR.x, ymaxR.y, ymaxR.width, ymaxR.height,
                    FXMLView.TEXT_Y_MAX);
            
        }catch(Exception e){
            fxmlView.displayError("Problem with reading scale ticks.");
        }

        try{
            Point xAxisCenter = new Point((xminR.x + xminR.width / 2 + xmaxR.x + xmaxR.width / 2) / 2,
                xminR.y + xminR.height);
            Rect xTitleR = descDetector.detectScaleDescription(
                xAxisCenter, DescriptionDetector.SCALE_X);
            if(xTitleR != null){
                fxmlView.setDescriptionRectangle(xTitleR.x, xTitleR.y, xTitleR.width, xTitleR.height,
                    FXMLView.TEXT_X_TITLE);
        }
        }catch (NullPointerException e){
            System.err.println("X rectangles missing");
        }
        try{
            Point yAxisCenter = new Point(yminR.x,
                (yminR.y + yminR.height / 2 + ymaxR.y + ymaxR.height / 2) / 2);
            Rect yTitleR = descDetector.detectScaleDescription(
                yAxisCenter, DescriptionDetector.SCALE_Y);
            if(yTitleR != null){
                fxmlView.setDescriptionRectangle(yTitleR.x, yTitleR.y, yTitleR.width, yTitleR.height,
                    FXMLView.TEXT_Y_TITLE);
        }
        }catch (NullPointerException e){
            System.err.println("Y rectangles missing");
        }

    }

    public Map<Double, String> handleLegend(int x, int y, int w, int h) {
        Map<Double, String> colourPlots = new HashMap<>();
        Rect roi = new Rect(x, y, w, h);
        Mat subImg = new Mat(plotModel.getSourceImage(), roi);
        ArrayList<Double> peaks = ColourSegmentation.process(subImg);
        plotModel.getPlotsInLegend().clear();
        for(Double d: peaks){
            plotModel.getPlotsInLegend().put(peaks.indexOf(d), 
                    new Color(Color.HSBtoRGB((float)(d/256.0f),0.5f,1.0f)));
            colourPlots.put(d, "Plot "+d+":");
        }
        removeAreaOnImage(x, y, w, h);
        plotModel.setLegendReaded(true);
        return colourPlots;
    }

    public void saveCSV(File file, int i) {

        Map<Integer, List<Point>> graphsPoints = ScaleConverter.countGraphPoints(plotModel);

        try{
            PrintWriter pw = new PrintWriter(file);
            StringBuilder dataFileContent = new StringBuilder();
            for (Point gp : graphsPoints.get(i)) {
                dataFileContent.append("\n");
                dataFileContent.append(gp.x);
                dataFileContent.append(",");
                dataFileContent.append(gp.y);
            }

            pw.print(dataFileContent.toString());
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GraphDigitizerDesktop.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Debug function
     */
    private void savePlotPoints(Rect areaRect) {
        BufferedImage tempImage = new BufferedImage(areaRect.width, areaRect.height,
                BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < areaRect.width; i++) {
            for (int j = 0; j < areaRect.height; j++) {
                tempImage.setRGB(i, j, new Color(255, 255, 255).getRGB());
            }
        }

        Map<Integer, List<Point>> plotValues = plotModel.getPlotValues();
        for (Integer c : plotValues.keySet()) {
            if (plotValues.get(c).size() > 20) {
//                System.out.print("Color(" + c.getRed() + "," + c.getGreen() + "," + c.getBlue() + "): ");
                for (Point p : plotValues.get(c)) {
                    if (p != null) {
//                        System.out.print("(" + p.x + "," + p.y + "), ");
                    }
                    try{
                    tempImage.setRGB((int) p.x, (int) p.y, new Color(0, 0, 0).getRGB());
                    }
                    catch(Exception e){
                        System.out.println(p.x + " " + p.y);
                    }
                }
                System.out.print("\n");
            }
        }
//        File outputfile = new File("resources/saved.png");
//        try {
//            ImageIO.write(tempImage, "png", outputfile);
//        } catch (IOException ex) {
//            Logger.getLogger(GraphDigitizerDesktop.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Removes part of image without displaing changes
     *
     * @param x
     * @param y
     * @param w
     * @param h
     */
    public void removeAreaOnImage(int x, int y, int w, int h) {
        Core.rectangle(plotModel.getSourceImage(), new Point(x, y),
                new Point(x + w, y + h), new Scalar(255, 255, 255), Core.FILLED);
//        sourceImageBuffered = ImageManager.mat2Img(sourceImage);
//        controller.setImageFromBuffered(sourceImageBuffered);
    }
}

