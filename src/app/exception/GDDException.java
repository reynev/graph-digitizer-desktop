/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.exception;

/**
 * Exception class for informing about exceptions in gdd.
 * @author reynev
 */
public class GDDException extends Exception{
    
    public GDDException(String msg){
        super(msg);
    }
}
