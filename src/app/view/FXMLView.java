/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.view;

import app.GraphDigitizerDesktop;
import app.exception.GDDException;
import app.preprocessing.Preprocessing;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ocr.OcrProcess;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import org.opencv.core.Rect;
import org.opencv.highgui.Highgui;

/**
 *
 * @author Drobny
 */
public class FXMLView implements Initializable {

    @FXML
    private ImageView imgView;
    @FXML
    private Button btnProcess;
    @FXML
    private Button btnApply;
    @FXML
    private TextField textTitle;
    @FXML
    private TextField textXTitle;
    @FXML
    private TextField textYTitle;
    @FXML
    private TextField textXMin;
    @FXML
    private TextField textXMax;
    @FXML
    private TextField textYMin;
    @FXML
    private TextField textYMax;
    @FXML
    private AnchorPane imagePane;
    @FXML
    private ToggleButton toggleSelecting;
    @FXML
    private ComboBox comboColors;
    @FXML
    private RadioButton radioPoint;
    @FXML
    private RadioButton radioLine;
    @FXML
    private CheckBox checkLegend;
    @FXML
    private Button btnReadLegend;
    @FXML
    private Button btnSavePlot;
    @FXML
    private Button btnProcessOCR;

    private Tooltip tooltip;

    private Stage stage;

    /**
     * TextField for which selecting is active
     */
    private TextField activeField=null;
    /**
     * Map with rectangles which are dedicated to chosen TextFields
     */
    private Map<TextField,Rectangle> selectingAreas=new HashMap<>();
    ;
    /** Current selected rectangle */
    private Rectangle selectRect;
    /**
     * Begin of current selected rect
     */
    private double xStart, yStart;
    /**
     * Original image size to displayed image size
     */
    private double resizeRatio;
    /**
     * Current action of selecting tool
     */
    private int currentAction=ACTION_NONE;
    /**
     * Selecting Tool asctions constants
     */
    private static final int ACTION_NONE=0;
    private static final int ACTION_REMOVE_LEGEND=1;
    private static final int ACTION_APPLY_SELECTING=2;
    /**
     * Text fields constants
     */
    public static final int TEXT_X_MIN=1;
    public static final int TEXT_X_MAX=2;
    public static final int TEXT_Y_MIN=3;
    public static final int TEXT_Y_MAX=4;
    public static final int TEXT_X_TITLE=5;
    public static final int TEXT_Y_TITLE=6;
    /**
     * Plot type constants
     */
    public static final int PLOT_POINT=11;
    public static final int PLOT_LINE=12;

    private GraphDigitizerDesktop gdd;

    /**
     * FileChooser for opening images (to save last location)
     */
    private final FileChooser fcOpen=new FileChooser();
    /**
     * FileChooser for saving results (to save last location)
     */
    private final FileChooser fcSave=new FileChooser();

    @Override
    public void initialize(URL url,ResourceBundle rb) {
        fcOpen.setTitle("Open Image File");
        fcOpen.setInitialDirectory(new File(".//"));
        fcOpen.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG","*.png"),
                new FileChooser.ExtensionFilter("JPG","*.jpg","*.jpeg"),
                new FileChooser.ExtensionFilter("BMP","*.bmp"),
                new FileChooser.ExtensionFilter("All files","*.*")
        );

        fcSave.setTitle("Save File");
        fcSave.setInitialDirectory(new File(".//"));
        fcSave.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV","*.csv")
        );
    }

    /**
     * Sets GraphDigitizer object in controller
     *
     * @param gdd
     */
    public void setGdd(GraphDigitizerDesktop gdd) {
        this.gdd=gdd;

        //temporary here
//        setImageFromBuffered(gdd.imageManager.sourceImageBuffered);
    }

    public void setImageFromBuffered(BufferedImage image) {
        WritableImage wImage=new WritableImage(image.getWidth(),image.getHeight());
        SwingFXUtils.toFXImage(image,wImage);
        imgView.setImage(wImage);
        imgView.fitWidthProperty().bind(stage.widthProperty());
        if (image.getWidth()>imgView.getFitWidth())
            resizeRatio=image.getWidth()/imgView.getFitWidth();
        else
            resizeRatio=image.getHeight()/imgView.getFitHeight();
    }

    @FXML
    private void processBtn(ActionEvent event) {
        textLostFocus();
        try {
            if (textXMin.getText().isEmpty()
                    ||textXMax.getText().isEmpty()
                    ||textYMin.getText().isEmpty()
                    ||textYMax.getText().isEmpty())
                throw new GDDException("Min/max values has to be filled.");
            gdd.process();
            btnSavePlot.setDisable(false);
        }
        catch (GDDException e) {
            displayError(e.getMessage());
        }
    }

    @FXML
    private void processOCR(ActionEvent event) {
        textLostFocus();
        TextField textField;
        Rectangle rectangle;
        Rect rect;
        int labelOrientation;
        String value;
        
        File imageFile;
        
        int i = 0;
        for(Map.Entry selectedArea : selectingAreas.entrySet()) {
            rectangle = (Rectangle) selectedArea.getValue();
            rect = new Rect((int) (rectangle.getX() * resizeRatio), (int) (rectangle.getY() * resizeRatio),
                    (int) (rectangle.getWidth() * resizeRatio), (int) (rectangle.getHeight() * resizeRatio));
            textField = (TextField) selectedArea.getKey();
            
            labelOrientation = OcrProcess.HORIZONTAL;
            /** Text/number OCR **/
            if(textField.equals(textTitle) || textField.equals(textXTitle) || textField.equals(textYTitle)) {
                if(textField.equals(textYTitle)) {
                    labelOrientation = OcrProcess.VERTICAL;
                }
                value = gdd.ocr.process(gdd.plotModel.getSourceImage().submat(rect), "all", labelOrientation);
            /** Only number OCR **/
            } else { 
                /** Vertical orientation if Y **/
                if(textField.equals(textYMin) || textField.equals(textYMax)) {
                    labelOrientation = OcrProcess.VERTICAL;
                }
//                imageFile = new File("./out"+ i++ +".png");
//                Highgui.imwrite(imageFile.getAbsolutePath(), gdd.plotModel.getSourceImage().submat(rect));
//                System.out.println(gdd.plotModel.getSourceImage().submat(rect).size());
                value = gdd.ocr.process(gdd.plotModel.getSourceImage().submat(rect), "number", labelOrientation);
            }
            textField.setText(value);
            System.out.println("VALUE: "+ value);
        }
    }

    /**
     * Debug event
     *
     * @param e
     */
    @FXML
    private void applySelectingAction(ActionEvent e) {
        switch (currentAction) {
            case ACTION_NONE:
                break;
            case ACTION_REMOVE_LEGEND:
                /*Map<Double, String> plots =*/
                gdd.handleLegend(
                        (int)(selectRect.getX()*resizeRatio),
                        (int)(selectRect.getY()*resizeRatio),
                        (int)(selectRect.getWidth()*resizeRatio),
                        (int)(selectRect.getHeight()*resizeRatio));
                break;
            case ACTION_APPLY_SELECTING:
                selectRect.setStroke(Color.RED);
                selectingAreas.put(activeField,selectRect);
                imagePane.getChildren().remove(selectRect);
                selectRect=null;
                removeAllRectangles();
                break;
        }
        disableSelecting();
        btnApply.setVisible(false);
    }

    /**
     * Enable selecting for removeing legend from img.
     *
     * @param e
     */
    @FXML
    private void readLegend(ActionEvent e) {
        textLostFocus();
        currentAction=ACTION_REMOVE_LEGEND;
        removeAllRectangles();
        enableSelecting();
        btnApply.setVisible(true);
        removeAllRectangles();
        showTooltip("Select area on image.");
    }

    /**
     * Action invoked when user focuses on TextField. It draws rectangle which
     * select ROI dedicated to selected TextField and it activate selecting
     * tool.
     *
     * @param e
     */
    @FXML
    private void selectArea(MouseEvent e) {
        if (activeField!=e.getSource()) {
            btnApply.setVisible(false);
            removeAllRectangles();
            activeField=(TextField)e.getSource();
            /*remove previous selecting*/
            imagePane.getChildren().remove(selectRect);
            selectRect=null;

            showTooltip("Select area on image or type value.");

            if (selectingAreas.containsKey(e.getSource()))
                //drawing red (saved) rectangle
                imagePane.getChildren().add(selectingAreas.get(e.getSource()));
            else {
                //show tooltip
            }
            enableSelecting();
            btnApply.setDisable(false);
            currentAction=ACTION_APPLY_SELECTING;
        }
    }

    @FXML
    private void loadImage(ActionEvent event) {
        textLostFocus();

        File file=fcOpen.showOpenDialog(stage);

        if (file!=null) {
            Action response=Dialogs.create()
                    .owner(stage)
                    .title("Preprocessing")
                    .masthead("Photo or matlab?")
                    .message("Does the file need preprocessing?")
                    .showConfirm();

            if (response==Dialog.Actions.YES) {
                gdd.loadImage(file.getPath());
                Preprocessing.process(gdd.plotModel);
                gdd.plotModel.setProcessing(true);
                gdd.findIntrestingAreasOnImage();
            }
            else if (response==Dialog.Actions.NO) {
                gdd.loadImage(file.getPath());
                gdd.findIntrestingAreasOnImage();
            }

        }
    }

    @FXML
    private void saveResults() {
        textLostFocus();

        File file=fcSave.showSaveDialog(stage);
        if (file!=null) {
            int plotNo=comboColors.getSelectionModel().getSelectedIndex();
            gdd.saveCSV(file,plotNo);
        }
    }

    @FXML
    private void changeLegendEnabled() {
        textLostFocus();
        if (checkLegend.isSelected())
            btnReadLegend.setDisable(false);
        else
            btnReadLegend.setDisable(true);
    }

    @FXML
    private void textTypeAction() {
        hideTooltip();
    }

    private void textLostFocus() {
        hideTooltip();
        disableSelecting();
        removeAllRectangles();
    }

    /**
     * Enables area selecting tool
     */
    private void enableSelecting() {
        imgView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                btnApply.setVisible(true);
                hideTooltip();
                xStart=e.getSceneX();
                yStart=e.getSceneY();
            }
        });
        imgView.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getSceneX()<imagePane.getWidth()
                        &&e.getSceneY()<imagePane.getHeight()) {
                    if (selectRect==null) {
                        selectRect=new Rectangle();
                        selectRect.setFill(null);
                        selectRect.setStroke(Color.BLUE);
                        imagePane.getChildren().add(selectRect);
                    }
                    double xS=Math.min(xStart,e.getSceneX());
                    double yS=Math.min(yStart,e.getSceneY());
                    double xE=Math.max(xStart,e.getSceneX());
                    double yE=Math.max(yStart,e.getSceneY());
                    selectRect.setX(xS);
                    selectRect.setY(yS);
                    selectRect.setWidth(xE-xS);
                    selectRect.setHeight(yE-yS);
                }
            }
        });
        imgView.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (Math.sqrt(Math.pow(xStart-e.getSceneX(),2)
                        +Math.pow(yStart-e.getSceneY(),2))<2) {
                    imagePane.getChildren().remove(selectRect);
                    selectRect=null;
                }
            }
        });
    }

    private void removeAllRectangles() {
        if (selectingAreas!=null)
            for (Rectangle r:selectingAreas.values())
                imagePane.getChildren().remove(r);
    }

    private void showTooltip(String msg) {
        if (tooltip!=null)
            tooltip.hide();
        tooltip=new Tooltip(msg);
        tooltip.show(stage);
    }

    private void hideTooltip() {
        if (tooltip!=null) {
            tooltip.hide();
            tooltip=null;
        }
    }

    /**
     * Disables selecting tool
     */
    private void disableSelecting() {
        imagePane.getChildren().remove(selectRect);
        selectRect=null;
        imgView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
            }
        });
        imgView.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
            }
        });
    }

    //Gettlers and settlers
    public void setDescriptionRectangle(int x,int y,int w,int h,int type) {
        Rectangle r = new Rectangle(x/resizeRatio,y/resizeRatio,w/resizeRatio,h/resizeRatio);
        r.setFill(null);
        r.setStroke(Color.RED);
        switch (type) {
            case TEXT_X_MIN:
                selectingAreas.put(textXMin,r);
                break;
            case TEXT_X_MAX:
                selectingAreas.put(textXMax,r);
                break;
            case TEXT_Y_MIN:
                selectingAreas.put(textYMin,r);
                break;
            case TEXT_Y_MAX:
                selectingAreas.put(textYMax,r);
                break;
            case TEXT_X_TITLE:
                selectingAreas.put(textXTitle,r);
                break;
            case TEXT_Y_TITLE:
                selectingAreas.put(textYTitle,r);
                break;
        }
    }

    public int getPlotType() {
        if (radioLine.isSelected())
            return PLOT_LINE;
        else if (radioPoint.isSelected())
            return PLOT_POINT;
        else
            return 0;
    }

    public double getXMinValue() {
        return Double.parseDouble(textXMin.getText());
    }

    public double getXMaxValue() {
        return Double.parseDouble(textXMax.getText());
    }

    public double getYMinValue() {
        return Double.parseDouble(textYMin.getText());
    }

    public double getYMaxValue() {
        return Double.parseDouble(textYMax.getText());
    }

    public void setStage(Stage stage) {
        this.stage=stage;
    }
    
    public Stage getStage(){
        return stage;
    }

    public void displayError(String msg) {
        Dialogs.create()
                .owner(stage)
                .title("Error!")
                .masthead(msg)
                .showError();
    }

    public void fillPlotCombobox(Map<Integer,java.awt.Color> plots) {
        for (Map.Entry<Integer,java.awt.Color> plot:plots.entrySet()) {
            Text item=new Text("------- Plot "+plot.getKey());
            item.setFont(Font.font(20));
            double r=plot.getValue().getRed()/255.;
            double g=plot.getValue().getGreen()/255.;
            double b=plot.getValue().getBlue()/255.;
            item.setFill(new Color(r,g,b,1));
            comboColors.getItems().add(item);
        }
    }

    public boolean hasLegend() {
        return checkLegend.isSelected();
    }
}
