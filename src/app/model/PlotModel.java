/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import app.processes.ThinningK3MProcess;
import app.processes.ZhangSuenThinning;
import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import utils.ImageManager;
import utils.ImageProcessesUtils;

/**
 *
 * @author reynev
 */
public class PlotModel {

    /**
     * Original plot image
     */
    private Mat sourceImage;
    private Mat preprocessedImage;

    private Map<Integer,List<Point>> plotValues;
    private Rect areaRect;
    private double zeroX;
    private double zeroY;
    private double pxToValX;
    private double pxToValY;
    private int plotType;
    private boolean legendReaded = false;
    private boolean processing = false;
    
    /**
     * List of points(ticks) on axis
     */
    private List<Point> xTicksList, yTicksList;
    private Map<Integer,Color> plotsInLegend=new HashMap<Integer,Color>();

    /**
     * Plot type constants
     */
    public static final int PLOT_POINT=11;
    public static final int PLOT_LINE=12;

    public PlotModel(Mat srcImg) {
        sourceImage = srcImg;
        computePreprocessedImage();
    }

    public boolean getProcessing(){
        return processing;
    }
    
    public void setProcessing(boolean bool){
        processing=bool;
    }
    
    public void setSourceImage(Mat mat){
        sourceImage = mat;
    }
    
    public Mat getSourceImage() {
        return sourceImage;
    }
    
    public void computePreprocessedImage(){
         preprocessedImage = ImageProcessesUtils.preparePreprocessedImage(sourceImage);
    }

    public void setAreaRect(Rect areaRect) {
        this.areaRect=areaRect;
    }

    public Rect getAreaRect() {
        return areaRect;
    }
    
    public void setPlotType(int type){
        plotType=type;
    }
    
    public int getPlotType(){
        return plotType;
    }


    public List<Point> getxTicksList() {
        return xTicksList;
    }

    public Map<Integer,Color> getPlotsInLegend() {
        return plotsInLegend;
    }

    public void setPlotsInLegend(Map<Integer,Color> plotsInLegend) {
        this.plotsInLegend=plotsInLegend;
    }

    public void setxyTicksList(List<Point> xTicksList,List<Point> yTicksList) {
        this.xTicksList=xTicksList;
        this.yTicksList=yTicksList;
    }

    public List<Point> getyTicksList() {
        return yTicksList;
    }

    public Map<Integer,List<Point>> getPlotValues() {
        return plotValues;
    }

    public void setScaleCoefficients(double zeroX,double zeroY,double pxToValX,double pxToValY) {
        this.zeroX=zeroX;
        this.zeroY=zeroY;
        this.pxToValX=pxToValX;
        this.pxToValY=pxToValY;
    }

    public double getZeroX() {
        return zeroX;
    }

    public double getZeroY() {
        return zeroY;
    }

    public double getPxToValX() {
        return pxToValX;
    }

    public double getPxToValY() {
        return pxToValY;
    }

    public void setPlotValues(Map<Integer,List<Point>> plotValues) {
        this.plotValues=plotValues;
    }

    public boolean isLegendReaded() {
        return legendReaded;
    }

    public void setLegendReaded(boolean legendReaded) {
        this.legendReaded = legendReaded;
    }
    
    public Mat getPreprocessedImage() {
        return preprocessedImage;
    }

    public void setPreprocessedImage(Mat preprocessedImage) {
        this.preprocessedImage = preprocessedImage;
    }
}
